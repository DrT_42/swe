package Implementation.presentationLeia;

import java.util.LinkedList;

import Implementation.applicationLeia.GameLogic.Field;
import Implementation.applicationLeia.GameLogic.GameAPIFactory;
import Implementation.applicationLeia.GameLogic.Gamer;
import Implementation.presentationLeia.Controller.Controller;

public class Haupt {

	public static void main(String args[]) {
		GameAPIFactory factory = GameAPIFactory.factory;
		LinkedList<Gamer> gamerList = new LinkedList<Gamer>();
		gamerList.add(new Gamer("Mr. Red", Field.StartfieldP1.getFieldPosition(), Field.HomefieldP1.getFieldPosition()));
		gamerList.add(new Gamer("Dr. Green", Field.StartfieldP2.getFieldPosition(), Field.HomefieldP2.getFieldPosition()));
		gamerList.add(new Gamer("Mrs. Blue", Field.StartfieldP3.getFieldPosition(), Field.HomefieldP3.getFieldPosition()));
		gamerList.add(new Gamer("Emperor Yellow", Field.StartfieldP4.getFieldPosition(), Field.HomefieldP4.getFieldPosition()));
		Controller controller = new Controller(factory.getModel(gamerList));
		while(true){
			controller.start();
		}
	}
}
