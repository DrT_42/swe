package Implementation.presentationLeia.Controller;

import java.util.Scanner;

import Implementation.applicationLeia.GameLogic.GameAPI;
import Implementation.presentationLeia.View.BattleGround;
import Implementation.applicationLeia.GameLogic.Info;
import Implementation.applicationLeia.GameLogic.Observer;

public class Controller {

	private GameAPI ga;
	private Observer<Info> bg;
	private Scanner s; 
	
	public Controller(GameAPI ga) {
				
		s = new Scanner(System.in);
		
		this.ga = ga;
		bg = new BattleGround(ga);
		ga.attach(bg);
	}
	
	
	public void start() {
		String aktuell = s.next();
		if (aktuell.equals("x")){
			ga.rollDice();
		}
		if (aktuell.equals("1")){
			ga.validateChoice(ga.getPlayTrainStatus().getCurrentGamer().getArmy().get(0));
		}
		if (aktuell.equals("2")){
			ga.validateChoice(ga.getPlayTrainStatus().getCurrentGamer().getArmy().get(1));
		}
		if (aktuell.equals("3")){
			ga.validateChoice(ga.getPlayTrainStatus().getCurrentGamer().getArmy().get(2));
		}
		if (aktuell.equals("y")){
			ga.executePlayTrain();
		}
	}
}
