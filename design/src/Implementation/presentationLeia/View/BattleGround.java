package Implementation.presentationLeia.View;

import java.util.Iterator;

import Implementation.applicationLeia.GameLogic.GameAPI;
import Implementation.applicationLeia.GameLogic.Gamer;
import Implementation.applicationLeia.GameLogic.Info;
import Implementation.applicationLeia.GameLogic.Observer;
import Implementation.applicationLeia.GameLogic.PlayTrainImpl;
import Implementation.applicationLeia.GameLogic.PlayTrainStatus;
import Implementation.applicationLeia.GameLogic.WisdomSoldier;

public class BattleGround implements Implementation.applicationLeia.BattleGround {

	private GameAPI model;
	
	public BattleGround(GameAPI ga){
		model = ga;
	}

	public void update(Info state) 
	{  
		System.out.println("Update received from Subject, state changed to : " + state);
        if (state == PlayTrainImpl.determined) {
        	System.out.println("Spielfelduebersicht: ");
        	for(Iterator<Gamer> git = model.getPlayTrainStatus().getGamerList().iterator(); git.hasNext();){
        		Gamer gamy = git.next();
        		System.out.println("Figuren des Spielers " + gamy.getName() + " : Figur 1: "+ gamy.getArmy().get(0).getPosition() + " Figur 2: "+ gamy.getArmy().get(1).getPosition() + " Figur 3: "+ gamy.getArmy().get(2).getPosition());
        	}
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " ist dran. Zum Wuerfeln 'x' druecken: ");
        }
        if (state == PlayTrainImpl.generated) {
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " hat eine " + model.getPlayTrainStatus().getDiceResult() + " gewuerfelt. Damit ist kein Zug moeglich.");
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " ist dran. Zum Wuerfeln 'x' druecken: ");
        }
        if (state == PlayTrainImpl.choose){
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " hat eine " + model.getPlayTrainStatus().getDiceResult() + " gewuerfelt.");
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " ist dran. Zum waehlen '1','2' oder '3' eingeben.");
        	System.out.println("Spielfelduebersicht: ");
        	for(Iterator<Gamer> git = model.getPlayTrainStatus().getGamerList().iterator(); git.hasNext();){
        		Gamer gamy = git.next();
        		System.out.println("Figuren des Spielers " + gamy.getName() + " : Figur 1: "+ gamy.getArmy().get(0).getPosition() + " Figur 2: "+ gamy.getArmy().get(1).getPosition() + " Figur 3: "+ gamy.getArmy().get(2).getPosition());
        	}
        }
        if (state == PlayTrainImpl.validated){
        	System.out.println("Spieler " + model.getPlayTrainStatus().getCurrentGamer().getName()+ " ist dran. Wahl mit 'y' bestaetigen oder zum waehlen '1','2' oder '3' eingeben.");
        }
	
	}

	/**
	 * @clientCardinality 4
	 * @link aggregation
	 */
	private Implementation.presentationLeia.View.StartingField lnkStartingField;
	/**
	 * @clientCardinality 12
	 * @link aggregation
	 */
	private Implementation.presentationLeia.View.HomeField lnkHomeField;
	/**
	 * @clientCardinality 16
	 * @link aggregation
	 */
	private Implementation.presentationLeia.View.Field lnkField;
	
}
