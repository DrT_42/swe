package Implementation.applicationLeia.GameLogic;

import java.util.LinkedList;

public class GameAPIFactoryImpl implements GameAPIFactory {

	/**
	 * @directed true
	 * @link composition
	 * @supplierRole playTrainImpl
	 */
	private PlayTrainImpl lnkPlayTrainImpl;
	/**
	 * @clientNavigability NAVIGABLE
	 * @directed true
	 * @link composition
	 * @supplierRole model
	 */
	
	private RulesOfWisdom lnkRulesOfWisdom;
	private static GameAPIFactoryImpl theFactory;
	protected LinkedList<Gamer> gamerList;
	
	private GameAPIFactoryImpl() {
	}

	public PlayTrainAPI getPlayTrainImpl(LinkedList<Gamer> gamer) {
		if (this.lnkPlayTrainImpl == null){
			this.lnkPlayTrainImpl = new PlayTrainImpl(gamer);
		}
		return lnkPlayTrainImpl;
	}

	static GameAPIFactory makeFactory() {
		if (GameAPIFactoryImpl.theFactory == null){
			GameAPIFactoryImpl.theFactory = new GameAPIFactoryImpl();
		}
		return GameAPIFactoryImpl.theFactory;
	}

	@Override
	public GameAPI getModel(LinkedList<Gamer> gamerList) {
		if (this.lnkRulesOfWisdom == null){
			this.gamerList = gamerList;
			this.lnkRulesOfWisdom = new RulesOfWisdom(this);
		}
		return lnkRulesOfWisdom;
	}
}
