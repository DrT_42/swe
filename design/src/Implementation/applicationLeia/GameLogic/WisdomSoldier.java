package Implementation.applicationLeia.GameLogic;

public class WisdomSoldier {


	private int position;
	
	public int getPosition() {
		return position;
	}
	
	public WisdomSoldier(int pos){
		this.position = pos;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public boolean isCruising() {
		return getPosition() >= Field.StartfieldP1.getFieldPosition();
	}
	
}