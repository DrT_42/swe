
package Implementation.applicationLeia.GameLogic;

import java.util.LinkedList;

public class Gamer {

	private String name;

	private int startFieldPosition;
	/**
	 * @directed true
	 * @supplierCardinality 4
	 */
	private WisdomStateArray lnkWisdomStateArray;
	/**
	 * @directed true
	 * @supplierCardinality 3
	 */
	private LinkedList<WisdomSoldier> army;
	
	public Gamer(String name, int SFPos, int HFPos){
		this.name = name;
		this.startFieldPosition = SFPos;
		army = new LinkedList<WisdomSoldier>();
		army.add(new WisdomSoldier(HFPos));
		army.add(new WisdomSoldier(HFPos));
		army.add(new WisdomSoldier(HFPos));
	}
	
	public String getName(){
		return name;
	}
	
	public WisdomStateArray getLnkWisdomStateArray() {
		return lnkWisdomStateArray;
	}
	public void setLnkWisdomStateArray(WisdomStateArray lnkWisdomStateArray) {
		this.lnkWisdomStateArray = lnkWisdomStateArray;
	}
	public LinkedList<WisdomSoldier> getArmy() {
		return army;
	}
	public void setArmy(LinkedList<WisdomSoldier> army) {
		this.army = army;
	}
	public int getStartFieldPosition() {
		return startFieldPosition;
	}
	public void setStartFieldPosition(int field) {
		this.startFieldPosition = field;
	}
	
	

}
