package Implementation.applicationLeia.GameLogic;

import java.util.Iterator;
import java.util.LinkedList;

public class PlayTrainStatus implements Status {

	/// result of the last dice roll in the current round
	private int diceResult;
	/// number of dicerolls in the current round
	private int numberOfDiceRolls;
	/// List of participating gamers
	private LinkedList<Gamer> gamerList;
	/// The gamer playing the current round
	private Gamer currentGamer;
	/// indicates whether the currently chosen WS can be moved.
	private boolean choiceValid;
	/// the WS chosen to be moved.
	private WisdomSoldier chosenOne;
	/// indicates whether the dice result allows for a valid move
	private boolean movePossible;
	
	public PlayTrainStatus(LinkedList<Gamer> gamers){
		diceResult =0;
		numberOfDiceRolls = 0;
		choiceValid = false;
		gamerList = gamers;
		currentGamer = gamers.getFirst();
		chosenOne= null;
		movePossible = false;
	}
	
	public PlayTrainStatus(PlayTrainStatus old){
		diceResult =0;
		numberOfDiceRolls = 0;
		gamerList = old.getGamerList();
		choiceValid = false;
		chosenOne = null;
		movePossible = false;
		for (Iterator<Gamer> it = old.getGamerList().iterator(); it.hasNext();)
		{
			Gamer gamy = it.next();
			if (gamy.equals(old.getCurrentGamer()))
			{
				if (it.hasNext())
				{
					currentGamer = it.next();
				} else
				{
					currentGamer = old.getGamerList().getFirst();
				}
				break;
			}
		}
	}
	
	public int getDiceResult() {
		return diceResult;
	}
	
	public void setDiceResult(int diceResult) {
		this.diceResult = diceResult;
	}
	
	public int getNumberOfDiceRolls() {
		return numberOfDiceRolls;
	}
	
	public void setNumberOfDiceRolls(int numberOfDiceRolls) {
		this.numberOfDiceRolls = numberOfDiceRolls;
	}
	
	public LinkedList<Gamer> getGamerList() {
		return gamerList;
	}
	
	public void setGamerList(LinkedList<Gamer> gamerList) {
		this.gamerList = gamerList;
	}
	
	public Gamer getCurrentGamer() {
		return currentGamer;
	}
	
	public void setCurrentGamer(Gamer currentGamer) {
		this.currentGamer = currentGamer;
	}

	public boolean isChoiceValid() {
		return choiceValid;
	}

	public void setChoiceValid(boolean movePossible) {
		this.choiceValid = movePossible;
	}

	public WisdomSoldier getChosenOne() {
		return chosenOne;
	}

	public void setChosenOne(WisdomSoldier chosenOne) {
		this.chosenOne = chosenOne;
	}

	public boolean isMovePossible() {
		return movePossible;
	}

	public void setMovePossible(boolean movePossible) {
		this.movePossible = movePossible;
	}
	
}
