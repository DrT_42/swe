package Implementation.applicationLeia.GameLogic;
public enum Field {
	
	HomefieldP1(-4),
	HomefieldP2(-3),
	HomefieldP3(-2),
	HomefieldP4(-1),
	StartfieldP1(0),
	StartfieldP2(5),
	StartfieldP3(10),
	StartfieldP4(15);
	
	private int fieldPosition;
	
	private Field(int fieldPosition) {
		this.fieldPosition = fieldPosition;
	}

	public int getFieldPosition() {
		return fieldPosition;
	}
	
}
