package Implementation.applicationLeia.GameLogic;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class PlayTrainImpl implements PlayTrainAPI {

	private PlayTrainStatus status;
	private Random randomGenerator;
	private Info state;

	public PlayTrainImpl(LinkedList<Gamer> gamers) {
		this.randomGenerator = new Random();
		this.status = new PlayTrainStatus(gamers);
	}
	
	public PlayTrainStatus getPlayTrainStatus(){
		return status;
	}
	
	@Override
	public void rollDice() {
		status.setDiceResult(randomGenerator.nextInt(6) + 1);
		status.setNumberOfDiceRolls(status.getNumberOfDiceRolls() + 1);
		for(Iterator<WisdomSoldier> it = status.getCurrentGamer().getArmy().iterator(); it.hasNext();)
		{
			if (it.next().isCruising()){
				status.setMovePossible(true);
				return;
			}
		}
		if(!status.isMovePossible()){
			if (status.getDiceResult()== 6){
				status.setMovePossible(true);
				return;
			}
		}
		// end of round.
		if (status.getNumberOfDiceRolls()==3){
			status = new PlayTrainStatus(status);
		}
	}

	@Override
	public void validateChoice(WisdomSoldier ws) {
		LinkedList<WisdomSoldier> army = status.getCurrentGamer().getArmy(); 
		assert army.contains(ws);
		int newPosition;
		
		if (ws.isCruising())
		{
			newPosition = (ws.getPosition() + status.getDiceResult())%20;
		} else
		{
			if (status.getDiceResult()!= 6)
			{
				status.setChoiceValid(false);
				return;
			}
			newPosition = status.getCurrentGamer().getStartFieldPosition();
		}
		
		
		for (Iterator<WisdomSoldier> iterator = army.iterator(); iterator.hasNext(); )
		{
			if (iterator.next().getPosition() == newPosition)
			{
				status.setChoiceValid(false);
				return;
			}
		}
		status.setChosenOne(ws);
		status.setChoiceValid(true);
	}


	@Override
	public void executePlayTrain() {
		assert status.isChoiceValid();
		
		if (status.getChosenOne().isCruising())
			status.getChosenOne().setPosition(status.getChosenOne().getPosition() + status.getDiceResult());
		else
			status.getChosenOne().setPosition(status.getCurrentGamer().getStartFieldPosition());
		
		// TODO: Insert check for collision here !!
		
		///prepare for next move:
		status = new PlayTrainStatus(status); 
	}
	
	public Info getState()
	{
		return state;
	}
}
