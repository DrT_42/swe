package Implementation.applicationLeia.GameLogic;
public interface Observer<INFO> {


	public void update(INFO info);
}
