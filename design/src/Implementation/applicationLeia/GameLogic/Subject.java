package Implementation.applicationLeia.GameLogic;
public interface Subject<INFO> {



	public void detach(Observer<INFO> obs);

	public void attach(Observer<INFO> obs);
}
