
package Implementation.applicationLeia.GameLogic;

public interface PlayTrainAPI {

	Info determined = new PlayTrainInfo();
	Info generated = new PlayTrainInfo();
	Info choose = new PlayTrainInfo();
	Info validated = new PlayTrainInfo();
	Info confirmed = new PlayTrainInfo();
	
	void rollDice();

	void validateChoice(WisdomSoldier ws);

	void executePlayTrain();
	
	public PlayTrainStatus getPlayTrainStatus();

}
