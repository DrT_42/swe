package Implementation.applicationLeia.GameLogic;

import java.util.LinkedList;

public interface GameAPIFactory {


	public GameAPIFactory factory = GameAPIFactoryImpl.makeFactory();


	public GameAPI getModel(LinkedList<Gamer> gamerList);
}
