
package Implementation.applicationLeia.GameLogic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class RulesOfWisdom implements GameAPI, PlayTrainAPI {

	/**
	* @directed true
	* @label gamerList<Gamer>
	* @supplierCardinality 1
	*/
	private LinkedList<Gamer> gamerList;
	
	/**
	* @clientNavigability NAVIGABLE
	* @directed true
	* @supplierRole currentInfo
	*/
	private Info currentInfo;
	
	/**
	 * @directed true
	 * @link aggregation
	 * @supplierRole playTrain
	 * @supplierVisibility private
	 */
	private PlayTrainAPI lnkPlayTrainAPI;
	
	/**
	* @clientNavigability NAVIGABLE
	* @directed true
	* @link association
	* @supplierRole factory
	*/
	private GameAPIFactoryImpl factory;
	
	private ArrayList<Observer<Info>> observers = new ArrayList<Observer<Info>>();
	
	public RulesOfWisdom(GameAPIFactoryImpl factory) {
		this.factory = factory;
		this.gamerList = factory.gamerList;
		this.lnkPlayTrainAPI = factory.getPlayTrainImpl(gamerList);
		this.currentInfo = PlayTrainImpl.determined;
	}

	public void attach(Observer<Info> obs) {
		this.observers.add(obs);
		obs.update(this.currentInfo);
	}

	public void detach(Observer<Info> obs) {
		this.observers.remove(obs);
	}
	
	public void notifyObservers(Info state) {
		Iterator<Observer<Info>> it = observers.iterator();
		while (it.hasNext()) {
			((Observer<Info>) it.next()).update(state);
		}
	}

	@Override
	public void rollDice() {
		if (this.currentInfo == PlayTrainImpl.determined 
				|| this.currentInfo == PlayTrainImpl.generated) {
			this.lnkPlayTrainAPI.rollDice();
			if (lnkPlayTrainAPI.getPlayTrainStatus().getDiceResult()==0){
				currentInfo = PlayTrainImpl.determined;
			}else if (lnkPlayTrainAPI.getPlayTrainStatus().isMovePossible()){
				this.currentInfo = PlayTrainImpl.choose;
			}else {
				this.currentInfo = PlayTrainImpl.generated;
			}
		}
		notifyObservers(this.currentInfo);
	}

	@Override
	public void validateChoice(WisdomSoldier ws) {
		if (this.currentInfo == PlayTrainImpl.choose || currentInfo == PlayTrainImpl.validated) {
			this.lnkPlayTrainAPI.validateChoice(ws);
			if (lnkPlayTrainAPI.getPlayTrainStatus().isChoiceValid())
				this.currentInfo = PlayTrainImpl.validated;
			else
				this.currentInfo = PlayTrainImpl.choose;
		}
		notifyObservers(this.currentInfo);
	}

	@Override
	public void executePlayTrain() {
		if (this.currentInfo == PlayTrainImpl.validated) {
			this.lnkPlayTrainAPI.executePlayTrain();
			this.currentInfo = PlayTrainImpl.determined;
		}
		notifyObservers(this.currentInfo);
	}

	@Override
	public PlayTrainStatus getPlayTrainStatus() {
		return this.lnkPlayTrainAPI.getPlayTrainStatus();
	}
}
